# DB for the COLib-19 project

## Run db

```
$ docker-compose -up
```

## Load data

```
$ ogr2ogr -append -f "PostgreSQL" PG:"host='0.0.0.0' port='5427' user='docker' dbname='mobilitydb' password='docker'" 2020-03-27_18-19_Fri.gpx
```